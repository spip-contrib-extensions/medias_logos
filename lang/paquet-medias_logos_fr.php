<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/medias_logos/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'medias_logos_description' => 'Ce micro plugin ajoute une petite fonctionnalité au plugin Médias :
un bouton qui permet d\'utiliser une image liée à un objet comme logo de ce dernier. Et c\'est tout !
<br>Concrètement, en cliquant sur le bouton, une copie de l\'image va être créée dans le dossier des logos, et utilisée comme logo pour l\'objet.
Ça ne change donc en rien la gestion des logos : c\'est juste un raccourci commode qui fait gagner un peu de temps.
Le logo n\'est pas lié au document : en cas de modification de ce dernier, le logo ne sera pas mis à jour.
<br>Utiliser les documents pour gérer les logos des objets est prévu pour une version ultérieure de SPIP : ce jour là, ce plugin sera obsolète.',
	'medias_logos_nom' => 'Logos Médias',
	'medias_logos_slogan' => 'Définir des documents comme logos d\'objets',

	// B
	'bouton_iconifier' => 'Utiliser comme logo',

);

?>